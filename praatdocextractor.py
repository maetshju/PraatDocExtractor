import sys, os, re
if(sys.argv[2].endswith('.praat')):
    print('The file you specified to write to may be your source code.')
    print('Proceed? (y/n)')
    if not input().lower().startswith('y'):
        sys.exit(0)
if os.path.isfile(sys.argv[2]):
    os.remove(sys.argv[2])
with open(sys.argv[1], 'r') as f:
    with open(sys.argv[2], 'w') as w:
        if len(sys.argv) == 4:
            w.write('# {}\n\n'.format(sys.argv[3]))
        else:
            w.write('# {}\n\n'.format(os.path.basename(sys.argv[1])))
        lines = iter(f.readlines())
        data = {'desc': [''], 'params': list(), 'calcs': list()}
        eof = False
        have_written_script_desc = False
        have_written_procedures_header = False
        curr_adding_to = 'desc'
        new_desc_next = False
        try:
            while not eof:
                line = next(lines)
                if line.startswith('#\''):
                    line = line[2:]
                    
                    if line.strip().startswith('@param'):
                        new_desc_next = False
                        curr_adding_to = 'params'
                        line = line.replace('@param', '*')
                        linelist = line.split()
                        linelist[1] = '*{}*'.format(linelist[1])
                        linelist[1] = linelist[1].replace('_', '\_')
                        linelist[1] = linelist[1].replace('$', '\$')
                        line = ' '.join(linelist)
                        data[curr_adding_to].append(line.strip())
            
                    elif line.strip().startswith('@calc'):
                        new_desc_next = False
                        curr_adding_to = 'calcs'
                        line = line.replace('@calc', '*')
                        linelist = line.split()
                        linelist[1] = '*{}*'.format(linelist[1])
                        linelist[1] = linelist[1].replace('_', '\_')
                        linelist[1] = linelist[1].replace('$', '\$')
                        line = ' '.join(linelist)
                        data[curr_adding_to].append(line.strip())
                    else:
                        list_index = max(0, len(data[curr_adding_to])-1)
                        if line.strip() == '':
                            new_desc_next = True

                        elif new_desc_next:
                            if curr_adding_to == 'desc':
                                data[curr_adding_to].append(line.strip())
                            else:
                                toadd = '> {}'.format(line.strip())
                                data[curr_adding_to].append(toadd)

                            new_desc_next = False

                        else:
                            curr = data[curr_adding_to][list_index]
                            repl = '{} {}'.format(curr, line.strip())
                            data[curr_adding_to][list_index] = repl
                elif line.strip().startswith('procedure'):
                    name = line.replace('procedure', '').strip()
                    namelist = name.split()
                    name = ' '.join(['*{}*'
                                     .format(n)
                                     if namelist.index(n)
                                     == len(namelist) - 1
                                     else '*{}*,'
                                     .format(n.replace(',', ''))
                                     if namelist.index(n) >= 1
                                     else n for n in namelist])
                    name = name.replace('_', '\_').replace('$', '\$')
                    if not have_written_procedures_header:
                        w.write('\n\n## Procedures')
                        have_written_procedures_header = True
                        
                    w.write('\n\n### {}\n\n'.format(name))
                    
                    currlen = len(data['desc'])
                    if currlen > 0:
                        if currlen > 1:
                            for d in data['desc'][:currlen-1]:
                                w.write('{}\n\n'.format(d.strip()))
                        w.write(data['desc'][currlen-1].strip())
                    
                    currlen = len(data['params'])
                    if currlen > 0:
                        w.write('\n\n#### Parameters\n\n')
                        if currlen > 1:
                            for p in data['params'][:currlen-1]:
                                w.write('{}\n\n'.format(p.strip()))
                        w.write(data['params'][currlen-1].strip())
                        
                    currlen = len(data['calcs'])
                    if currlen > 0:
                        w.write('\n\n#### Calculates\n\n')
                        if currlen > 1:
                            for c in data['calcs'][:currlen-1]:
                                w.write('{}\n\n'.format(c.strip()))
                        w.write(data['calcs'][currlen-1].strip())
                            
                    data = {'desc': [''], 'params': list(), 'calcs': list()}
                    name = ''
                    curr_adding_to = 'desc'
                    new_desc_next = False
                elif not line.strip() == '' and not line.strip().startswith('#'):
                    
                    currlen = len(data['desc'])
                    if currlen > 0:
                        if currlen > 1:
                            for d in [d for d in data['desc'][:currlen-1]
                                if not d == '']:
                                w.write('{}\n\n'.format(d.strip()))
                        w.write(data['desc'][currlen-1].strip())
                    
                    currlen = len(data['params'])
                    if currlen > 0:
                        w.write('\n\n## Arguments to the Script\n\n')
                        if currlen > 1:
                            for p in data['params'][:currlen-1]:
                                w.write('{}\n\n'.format(p.strip()))
                        w.write(data['params'][currlen-1].strip())
                        
                    currlen = len(data['calcs'])
                    if currlen > 0:
                        w.write('\n\n## Script Calculations\n\n')
                        if currlen > 1:
                            for c in data['calcs'][:currlen-1]:
                                w.write('{}\n\n'.format(c.strip()))
                        w.write(data['calcs'][currlen-1].strip())
                    have_written_script_desc = True
                    data = {'desc': [''], 'params': list(), 'calcs': list()}
                    name = ''
                    curr_adding_to = 'desc'
                    new_desc_next = False
        except StopIteration:
            eof = True
            w.write('\n')
            
            if not have_written_script_desc:
                if len(data['desc']) > 0:
                    for d in data['desc']:
                        w.write('{}\n\n'.format(d.strip()))
                
                if len(data['params']) > 0:
                    w.write('## Arguments to the Script\n\n')
                    for p in data['param']:
                        w.write('{}\n\n'.format(p.strip()))
                        
                if len(data['calcs']) > 0:
                    w.write('## Script Calculations\n\n')
                    for c in data['calc']:
                        w.write('{}\n\n'.format(c.strip()))
