# Copyright 2016 Matthew C. Kelley
# Written at the University of Alberta
#
# A sample Praat script to demonstrate the PraatDocExtractor
#
# This file is part of PraatDocExtractor.
#
# PraatDocExtractor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, version 2.1.
#
# PraatDocExtractor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License, version 2.1
# along with PraatDocExtractor. If not, see <http://www.gnu.org/licenses/>.

#' This is a sample script for the document generation.
#'
#' We can see that there will be some line breaks here.
#'
#'
#' This is testing how many line breaks you can put in.
#'
#'
#' @param num1 A positive real number that 3 will be added to
#' @param blahblah$ A string that will be printed out and doubled
#'
#' @calc mysum The sum of `num1` and 3
#' @calc mystring$ The `blahblah` variable with the string "blah" at the end of it

################
# script.praat #
################

form Sample form
    positive num1 3
    text blahblah test
endform

var = 2+3
# Comments that will be
s$ = "hello"
# Ign
# oredddd

@test: num1, blahblah$
mysum = test.val

@test2: "Hello, world!"

@test3: blahblah$
mystring$ = test2.val$

#' A sample procedure that performs simple calculations.
#'
#' @param .var The value to add three to.
#' 
#' Here's more information about `.var`
#'
#'
#' And more...
#' 
#' @calc .val The calculated result of adding 3 to `.var`.
#' @calc .out$ A repetition of the `.p$` variable.
#'
#' @param .p$ The string value to be repeated.
procedure test: .var, .p$
    .intermediate = .var + 1
    .val = .intermediate + 2
    .out$ = .p$ + .p$
endproc

#' Writes the passed in string value or variable to the info line
#'
#' @param .val$ String to be written to the info line.
procedure test2: .val$
    writeInfoLine: .val$
endproc

#' A third procedure! It concatenates a passed value and "blah"
#'
#' It also contains a multiparagraph and multiline
#' description.
#'
#' @param .my$ The string to add "blah" to.
#'
#' @calc .val$ The concatenation of `.my$` and "blah"
procedure test3: .my$
    .val$ = .my$ + "blah"
endproc
