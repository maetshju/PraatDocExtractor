'''
Copyright 2016 Matthew C. Kelley
Written at the University of Alberta

A simple script to create the Python code tangle from praatdocextract.md

This file is part of PraatDocExtract.

PraatDocExtract is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 2.1.

PraatDocExtract is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License, version 2.1
along with PraatDocExtract. If not, see <http://www.gnu.org/licenses/>.
'''
import sys

with open(sys.argv[1], 'r') as f:
    with open(sys.argv[2], 'w') as w:

        lines = [line for line in f.readlines()]
    
        in_code_portion = False
    
        for line in lines:
        
            if not in_code_portion and line.strip() == '```python':
            
                in_code_portion = True
                
            elif in_code_portion:
            
                if not line.strip() == '```':
                
                    w.write(line)
                    
                else:
                
                    in_code_portion = False
