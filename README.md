# This program is made available to you under the LGPL version 2.1, and not GPLv2 as GitLab indicates above.

# PraatDocExtractor v1.1.6

**Note:** This document is written in [Pandoc Markdown](http://pandoc.org). It has features that may not be available in some Markdown interpreters, so the formatting may be off when interpreted by non-Pandoc interpreters (such as this one on GitLab).

PraatDocExtractor is a Python 3 program (licensed with the LGPLv2.1) that will generate Praat script documentation from special comments within the Praat script. The program is written in a style inspired by literate programming. To read about the program and how it works, read the `praatdocextractor.md` file. A weave is provided in `praatdocextractor.html` and `praatdocextractor.tex`.

```bash
pandoc praatdocextractor.md -o praatdocextractor.pdf
```

Additionally, the tangle, contained in `praatdocextractor.py`, can be run with the folllwing command:

```bash
python praatdocextractor.py {praat\_file} {markdown\_file} ({title})
```

where

* \{praat\_file\} is the Praat file to extract the documentation from

* \{markdown\_file\} is the file to write the Markdown documentation to

* ({title}) is the title to write into the documentation, which is optional to pass in, as the program will use the base file name as the title if this argument is not present

Additionally, the weave files, `praatdocextract.html` and `praatdocextract.tex`, was generated from the Markdown using [Pandoc](http://pandoc.org/)

```bash
pandoc -s praatdocextractor.md -o praatdocextractor.html
```

```bash
pandoc -s praatdocextractor.md -o praatdocextractor.tex
```

# Changelog

## 1.1.6

Released 16 September 2016

Fixed a bug about a string not being terminated in the Python code which was only triggered when passing in a separate file name to the program.

## 1.1.5

Released 17 August 2016

Added the ability to specify the title of the document in the Python version of the code, which was already a feature in the Praat script version. Additionally, the program has been ported to a Praat script version that can be run from within Praat itself.

## 1.1.4

Released 13 August 2016

Fixed two bugs. The first was that a single line break was interpreted as a paragraph break due to a flag not getting turned off when the description was done being read. The second was that if the name of a parameter or calculation showed up in its description, it would be italicized by the program due to the way the formatting code was written. A less golfy approach was taken, which is clearer and fixes the bug as well.

## v1.1.3

Released 13 August 2016

Fixed a bug where the underscores and dollar signs weren't escaped in the Markdown output in the procedure signatures.

## v1.1.2

Released 12 August 2016

Renamed files to match with project name PraatDocExtractor and changed all instances of PraatDocExtract within them to PraatDocExtractor.

## v1.1.1

Released 12 August 2016

Fixed a slight whitespace formatting error so that there is not a bunch of newline characters at the end of the generated Markdown file now. There should only be one now.

## v1.1

Released 12 August 2016

Completed a refactor that allows for `@param` and `@calc` tags in the general script description comments so as to permit the documentation of a form that takes arguments for the script. Additionally, this changes the comment grammar to be more general and not need to account for the state of the program (having encountered general script description or not).

## v1.0

Released 12 August 2016

Initial release of program. Supports general script descriptions, as well as descriptions of procedure APIs with `@param` tags for parameters and `@calc` for calculations.
