# Copyright 2016 Matthew C. Kelley
# Written at the University of Alberta
#
# A Praat script version of the PraatDocExtractor program
#
# This file is part of PraatDocExtractor.
#
# PraatDocExtractor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, version 2.1.
#
# PraatDocExtractor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License, version 2.1
# along with PraatDocExtractor. If not, see <http://www.gnu.org/licenses/>.

#' Praat script version of the program to extract documentation
#' from Praat script.
#'
#' @param filename The absolute filename of the Praat script to extract
#' the documentation from.
#'
#' @param output_file The absolute filename of the Markdown file to be
#' generated. This file will be appended to if it already exists, rather than
#' overwritten.
#'
#' @param title_of_document Title of the documentation to be written to the
#' Markdown file. Will be the only level-one header in the document.
form PraatDocExtractor
	sentence Filename script.praat
	sentence Output_file script.md
	sentence Title_of_document script.praat
endform

# Read in information from text file
lines = Read Strings from raw text file: filename$
numlines = Get number of strings

# Set up environment for running through the lines of text
numdesc = 0
numparam = 0
numcalc = 0
curr_add$ = "desc"
new_desc_next = 0
have_written_proc_header = 0

# Write the header to file
fileappend "'output_file$'" # 'title_of_document$''newline$''newline$'

# Iterate through the lines of the file and parse the comments out
for ifile to numlines

	# Get next line and check for tags
	selectObject: lines
	line$ = Get string: ifile
	linelen = length(line$)
	@startswith: line$, "#'"
	commstarts = startswith.starts

	@startswith: line$, "procedure"
	procstarts = startswith.starts

	@startswith: line$ , "#"
	regcommstarts = startswith.starts

	@strip: line$
	if strip.stripped$ = ""
		blank = 1
	else
		blank = 0
    endif

    # Comment starts with indicator to extract it
	if commstarts = 1
		line$ = right$(line$, linelen - 2)
		@strip: line$
		stripped$ = strip.stripped$

		@startswith: stripped$, "@param"
		paramstarts = startswith.starts

		@startswith: stripped$, "@calc"
		calcstarts = startswith.starts

        # Comment contains parameter marker
        # Extract information, format it, and store it
		if paramstarts = 1

			numparam = numparam + 1
			new_desc_next = 0
			curr_add$ = "params"
			stripped$ = right$(stripped$, length(stripped$) - 6)
			@strip: stripped$
			stripped$ = strip.stripped$
			var_name_length = index(stripped$, " ") - 1
			var_name_italicized$ = "*" + left$(stripped$, var_name_length) + "*"
			var_name_italicized$ = replace$(var_name_italicized$, "_", "\_",0)
			var_name_italicized$ = replace$(var_name_italicized$, "$", "\$",0)
			stripped$ = "* " + var_name_italicized$ + right$(stripped$, length(stripped$) - var_name_length)
			params$[numparam] = stripped$
		
		# Comment contains calculation marker
		# Extract information, format it, and store it	
		elsif calcstarts = 1

			numcalc = numcalc + 1
			new_desc_next = 0
			curr_add$ = "calcs"
			stripped$ = right$(stripped$, length(stripped$) - 6)
			@strip: stripped$
			stripped$ = strip.stripped$
			var_name_length = index(stripped$, " ") - 1
			var_name_italicized$ = "*" + left$(stripped$, var_name_length) + "*"
			var_name_italicized$ = replace$(var_name_italicized$, "_", "\_",0)
			var_name_italicized$ = replace$(var_name_italicized$, "$", "\$",0)
			stripped$ = "* " + var_name_italicized$ + right$(stripped$, length(stripped$) - var_name_length)
			calcs$[numcalc] = stripped$

		# Comment does not contain a tag
		# Add as description to whatever section is being added to
		else
		
		    # Checks to keep from indexing with a negative number
            if curr_add$ = "param"
				list_index = max(0, numparam - 1)
			elsif curr_add$ = "calcs"
				list_index = max(0, numcalc - 1)
			elsif curr_add$ = "desc"
				list_index = max(0, numdesc - 1)
			endif

            # Whitespace found
            # Next piece of text should be descriptive
			if stripped$ = ""
				new_desc_next = 1
				
			# Add descriptive text to portion currently being worked on
			elsif new_desc_next = 1
				if curr_add$ = "desc"
					numdesc = numdesc + 1
					desc$[numdesc] = stripped$
				elsif curr_add$ = "params"
					toadd$ = "> " + stripped$
					numparam = numparam + 1
					params$[numparam] = toadd$
				else
					toadd$ = "> " + stripped$
					numcalc = numcalc + 1
					calcs$[numcalc] = toadd$
                endif
				new_desc_next = 0
				
			# Append to end of current item, or add first item if empty
			else
				if curr_add$ = "desc"
					if numdesc = 0
                        numdesc = numdesc + 1
                        desc$[numdesc] = stripped$
					else
                        curr$ = desc$[numdesc]
                        curr$ = curr$ + " " + stripped$
                        desc$[numdesc] = curr$
                    endif
				elsif curr_add$ = "params"
                    if numparam = 0
                        numparam = numparam + 1
                        params$[numparam] = stripped$
                    else
                        curr$ = params$[numparam]
                        curr$ = curr$ + " " + stripped$
                        params$[numparam] = curr$
                    endif
				else
					if numcalc = 0
                        numcalc = numcalc + 1
                        calcs$[numcalc] = stripped$
                    else
                        curr$ = calcs$[numcalc]
                        curr$ = curr$ + " " + stripped$
                        calcs$[numcalc] = curr$
                    endif
				endif
			endif
        endif
    
    # Procedure found
    # Create header, then write procedure documentation to file
    elsif procstarts = 1
        @make_proc_heading: right$(line$, length(line$) - length("procedure "))
        name$ = make_proc_heading.final$
        
        if have_written_proc_header = 0
            fileappend "'output_file$'" 'newline$''newline$'## Procedures
            have_written_proc_header = 1
        endif

        fileappend "'output_file$'" 'newline$''newline$'### 'name$''newline$''newline$'

        if numdesc > 0
            if numdesc > 1
                for d to numdesc - 1
                    @strip: desc$[d]
                    dstripped$ = strip.stripped$
                    fileappend "'output_file$'" 'dstripped$''newline$''newline$'
                endfor
            endif
            finaldesc$ = desc$[numdesc]
            fileappend "'output_file$'" 'finaldesc$'
            numdesc = 0
        endif

        if numparam > 0
        fileappend "'output_file$'" 'newline$''newline$'#### Parameters'newline$''newline$'
            if numparam > 1
                for p to numparam - 1
                    @strip: params$[p]
                    pstripped$ = strip.stripped$
                    fileappend "'output_file$'" 'pstripped$''newline$''newline$'
                endfor
            endif
            finalparam$ = params$[numparam]
            fileappend "'output_file$'" 'finalparam$'
            numparam = 0
        endif

        if numcalc > 0
        fileappend "'output_file$'" 'newline$''newline$'#### Calculates'newline$''newline$'
            if numcalc > 1
                for c to numcalc - 1
                    @strip: calcs$[c]
                    cstripped$ = strip.stripped$
                    fileappend "'output_file$'" 'cstripped$''newline$''newline$'
                endfor
            endif
            finalcalc$ = calcs$[numcalc]
            fileappend "'output_file$'" 'finalcalc$'
            numcalc = 0
        endif

        curr_add$ = "desc"
        new_desc_next = 0

	# First line of non-comment code found
	# Write script documentation to file
	elsif blank = 0 and regcommstarts = 0

		if numdesc > 0
			if numdesc > 1
				for d to numdesc - 1
					@strip: desc$[d]
					dstripped$ = strip.stripped$
					if dstripped$ != ""
						fileappend "'output_file$'" 'dstripped$''newline$''newline$'
					endif
                endfor
			endif
			finaldesc$ = desc$[numdesc]
			fileappend "'output_file$'" 'finaldesc$'
			numdesc = 0
		endif

		if numparam > 0
			fileappend "'output_file$'" 'newline$''newline$'## Arguments to the Script'newline$''newline$'
			if numparam > 1
				for p to numparam - 1
					@strip: params$[p]
					pstripped$ = strip.stripped$
					fileappend "'output_file$'" 'pstripped$''newline$''newline$'
                endfor
			endif
			finalparam$ = params$[numparam]
			fileappend "'output_file$'" 'finalparam$'
			numparam = 0
		endif

		if numcalc > 0
			fileappend "'output_file$'" 'newline$''newline$'## Script Calculations'newline$''newline$'
			if numcalc > 1
				for c to numcalc - 1
					@strip: calcs$[c]
					cstripped$ = strip.stripped$
					fileappend "'output_file$'" 'cstripped$''newline$''newline$'
                endfor
			endif
			finalcalc$ = calcs$[numcalc]
			fileappend "'output_file$'" 'finalcalc$'
			numcalc = 0
		endif

		curr_add$ = "desc"
		new_desc_next = 0
	endif
endfor

fileappend "'output_file$'" 'newline$'

Remove

#' Remove leading and trailing whitespace from a string
#'
#' @param .s$ String to remove whitespace from
#'
#' @calc .stripped$ String with whitespace removed
procedure strip: .s$
    
	.len = length(.s$)
	.lindex = index_regex(.s$, "[^ \n\t\r]")
	.beginning$ = right$(.s$, .len - .lindex + 1)
	.rindex = index_regex(.beginning$, "[ \n\t\r]*$")
	.stripped$ = left$(.beginning$, .rindex-1)
endproc

#' Check if passed string starts with a certain string of characters or not
#'
#' @param .s$ String to check the start of
#'
#' @param .start$ String of characters to check for in `.s$`
#'
#' @calc .starts Integer representing a boolean value of whether `.s$` started
#' with `.start$`. A value of `1` means true, and a value of `0` means false.
procedure startswith: .s$, .start$
	.len = length(.start$)
	.check$ = left$(.s$, .len)
	if .check$ = .start$
		.starts = 1
	else
		.starts = 0
	endif
endproc

#' Create the heading for a procedure, including formatting and whitespace
#'
#' @param .s$ The first line of a procedure declaration from which the
#' the header is to be derived.
#'
#' @calc .final$ The final result of the formatting, including escaping
#' underscores and dollar signs, separating the arguments by commas, and
#' putting argument names in italics.
procedure make_proc_heading: .s$
	.i = index(.s$, ":")
	.name$ = left$(.s$, .i)
	.args$ = right$(.s$, length(.s$) - .i - 1)
	@strip: .args$
	.temp$ = strip.stripped$
	.final_builder$ = ""
	.word_builder$ = "*"
	.arglen = length(.args$)
	for .argi to .arglen
		.curr_char$ = left$(.temp$, 1)
		if .argi = .arglen
			.final_builder$ = .final_builder$ + .word_builder$ + .curr_char$ + "*"
		elsif .curr_char$ = " "
			.word_builder$ = .word_builder$ + "*"
			.final_builder$ = .final_builder$ + .word_builder$ + ", "
			.word_builder$ = "*"
		elsif .curr_char$ != ","
			.word_builder$ = .word_builder$ + .curr_char$
		endif
		.temp$ = right$(.temp$, length(.temp$) - 1)
	endfor
	.final$ = .name$ + " " + .final_builder$
	.final$ = replace$(.final$, "_", "\_", 0)
	.final$ = replace$(.final$, "$", "\$", 0)
endproc
