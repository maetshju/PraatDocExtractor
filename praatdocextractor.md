---
title: PraatDocExtractor
author: Matthew C. Kelley
header-includes:
    - \usepackage[margin=1in]{geometry}
---

# License Information

```
Copyright 2016 Matthew C. Kelley
Written at the University of Alberta

A Markdown file containing the PraatDocExtractor program in a literate programming style.

This file is part of PraatDocExtractor.

PraatDocExtractor is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 2.1.

PraatDocExtractor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License, version 2.1
along with PraatDocExtractor. If not, see <http://www.gnu.org/licenses/>.
```

# Introduction

Good scientific research hinges on being open, sharing work with others, and illuminating the knowledge we have yet to uncover. This includes sharing code that we develop and providing good documentation for it in the hope that other researchers, and perhaps the public at large, may find it useful. However, for those of us for whom scripting in Praat is a part of our toolchain, there is no established documentation standard for Praat script documentation. Nor is there a style guide to suggest what might be useful to document. As scientists, this should concern us if we are ever to share our Praat scripts with others for them to be able to use, build, and modify. And indeed, it runs contrary to the ideal of openness that science foments in our work. Providing documentation for a Praat script and the procedures it provides should be helpful for both the developer, should they need to revisit a script later, and for any users of the Praat script.

The solution presented here describes a simple documentation system and provides a Python implementation of a parser to pull the documentation out of the Praat source file and generate a file in the [Pandoc dialect](http://pandoc.org/MANUAL.html#pandocs-markdown) of Markdown with the documentation contained inside. Because there is not yet a standard, common implentation of Markdown, selecting the Pandoc dialect of Markdown may mean that there are incompatibilities with other Markdown dialects, such as the GitLab website this program is hosted on, which does not require escaping dollar signs and which displays block quotes differently than Pandoc does in its output formats like HTML and PDF, making the documentation for the Praat script render weird in their implementation of Markdown. Until such as time as Markdown is standardized, this is an unavoidable problem.

This documentation is approximately an API for the script, since it documents the script's overall purpose and whatever procedures exist within the script. This is not a panacea to documentation in Praat scripts (which is, to be fair, an issue with software development in general), but rather a tool that can be used in the process of bettering our standards of documentation.

The Python code is presented in a manner that is similar in spirit to Donald Knuth's idea of [literate programming](https://en.wikipedia.org/wiki/Literate_programming) in that the code is intermixed with the documentation. The results of both the tangle and the weave are included in this distribution of the program. The tangle is produced from a custom Python script that pulls out the code that is marked as Python code in a Pandoc Markdown file; this Python script is included in this distribution as well. Additionally, the weave is included as a TeX file and an HTML file and has been generated through Pandoc's Markdown to TeX and Markdown to HTML conversion.

All the code provided in this distribution is provided to you under the terms of the GNU Lesser General Public License, v2.1. Further information can be found at the bottom of this document. Terms of the GNU Lesser General Public License, v2.1 can be found in the file "COPYING.LESSER" or on [the GNU website](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html), and terms of the GNU General Public License, v2 can be found in the file "COPYING".

## Running the Program

These instructions will be repeated below when appropriate, but to create the tangle from this file, run

```bash
python extractcode.py {md\_file} {python\_file} ({title})
```

where

* \{md\_file\} is the Markdown file you're pulling the Python code from. In this case, it would be this file.

* \{python\_file\} is the Python script to be generated. You can call it what you like.

* ({title}) is the title to write into the documentation, which is optional to pass in, as the program will use the base file name as the title if this argument is not present

This will generate the Python script that will be used to create the Praat documentation.

Now, to run the documentation generator, presuming you named the Python script `praatdocextractor.py`

```bash
python praatdocextractor.py {praat\_file} {markdown\_file}
```

where

* \{praat\_file\} is the Praat file to extract the documentation from

* \{markdown\_file\} is the file to write the Markdown documentation to

# The Documentation Structure

The structure for the documentation described for use with this parsing and extracting program is modeled after [Doxygen](http://www.stack.nl/~dimitri/doxygen/), using [Roxygen's](http://roxygen.org/) comment delimiter "`#'`". Specifically, the documentation for a script contains:

* A description of what the script does as a whole

* A description of each procedure, including:
    * The name of the procedure
    * A general description of what the procedure does
    * The list of parameters the procedure requires
    * What each parameter in the procedure is
    * What the procedure calculates as an end result
 
The descriptions are assumed to be written in Pandoc Markdown with no preceding tags, whereas each parameter's description is preceded by an `@param` tag (short for "parameter"), and each calculation's description is preceded by an `@calc` tag (short for "calculation"). It's worth mentioning that "calculation" was chosen instead of "return" because the procedures in a Praat script do not return any values and instead merely add variables to the environment that carry the calculations and assignments described in the procedure.

The descriptions may use Pandoc Markdown formatting, but must be escaped as normal, **with the exception of underscores, "\_", and dollar signs, "\$", which occur in variable names directly after a tag, which the program will escape for you. Everything else must be escaped if it would normally be escaped in Pandoc Markdown**.

## Grammatical Definition of a Comment

There are two different kinds of comments that can be processed by the parser:

1. **Script and procedure description comments**: These kinds of comments are the first found in the script and serve to describe what the script itself does. The parser will read this kind of comment until it gets to the first bit of Praat code after the comment lines for the script description, and until it gets to the first `@param` or `@calc` tag in the procedure descriptions. Paragraph breaks will be respected in the resulting Markdown file.

> These comments are separated from each other by two or more newline characters (representing a paragraph break).

2. **Tag description comments**: These kinds of comments happen after the description comments, and are invoked by the presence of a tag in the comment. These comments will begin with `@param` or `@calc`, and the text following the tag is added to the text description of the tag, where paragraph breaks will be respected. The parser will read through text until either the next tag is found, the keyword `procedure` is found, or a piece of non-comment Praat code is found, indicating that the description of the script in general or the current `procedure` is finished.

> These comments are separated from each other by the occurrence of the next tag.

These definitions are structured to produce the following entailments about the parser:

1. When processing the script description text, paragraph breaks made up of two or more newline characters are respected in the output Markdown file, so long as they have the comment indicator preceding them. They will only be rendered as two newline characters, however, regardless of whether or not more than two were used to break up the paragraphs.

2. Once the parser begins processing tagged items, no more description text will be able to be added to the script description portion. Instead, new description text found will be associated with a procedure in general.

3. Paragraph breaks made up of two or more newline characters with commenting indicators between tagged items will not instruct the parser to begin a new tagged item, but rather, to add the following paragraph to the description currently being constructed. This allows for multi-paragraph descriptions of parameters or calculations.

4. Line breaks while within the same paragraph to prevent a line from spanning too far will not result in line breaks within the generated Markdown file, as seems the de facto standard in Markdown.

5. A form that takes arguments for the scripts can be documented during the general script description in the same way as procedures can be later on.

The grammar of a comment is produced in a context-free grammar format below, though the parser is iterative and not recursive, so this is provided for purposes of convenience:

```
comment --------> ""
----------------> indicator
----------------> indicator element
element --------> description
----------------> tag variable tagged
description ----> text newline
----------------> nontag text newline indicator description
tagged ---------> text newline
----------------> text [newline indicator]+ nontag tagged
```

where strings are delimited with double-quote marks, and with the following primitives:

* **newline** A newline character
* **indicator** The character string "`#'`"
* **text** Any string of one or more text characters
* **tag** Any of the tags available for use in the comments:
    * `@param` To denote parameters for procedures
    * `@calc` To denote calculations for procedures
* **nontag** Any string of one or more non-whitespace text characters such that this string does not match one of the pre-defined tags
* **variable** The variable name of the parameter or calculation being described, which does not need its underscores or dollar signs escaped
    
and where square brackets, `[` and `]`, denote a grouping and the `+` indicates that there are one or more instances of the preceding item.

# The Python Parser

The parser, called `praatdocextractor.py`, can be called like so

```bash
python praatdocextractor.py {praat\_file} {markdown\_file} ({title})
```

where

* \{praat_file\} is the Praat file to extract the documentation from

* \{markdown_file\} is the file to write the Markdown documentation to

* ({title}) is the title to write into the documentation, which is optional to pass in, as the program will use the base file name as the title if this argument is not present

The user is expected to provide appropriate file extensions for the file, and the parser and will not be able to locate a file in the command line arguments should it not be correctly named.

## Getting started

The parser for the code is written in Python 3, and makes use of the `sys`, `os`, and `re` modules:


```python
import sys, os, re
```

The parser first performs a check to see if the user has ordered the files correctly. This is a rudimentary check, but provided the user has named their Praat files according to convention and used the ".praat" file extension, the check should work. If the check detects that the user may have specified the program to write into the Praat code, it will ask for user permission to continue, and exit if that permission is not given.


```python
if(sys.argv[2].endswith('.praat')):
    print('The file you specified to write to may be your source code.')
    print('Proceed? (y/n)')
    if not input().lower().startswith('y'):
        sys.exit(0)
```

The parser then checks to see if there is an existing documentation file with the same name as the one passed into the program. It will be removed if the check returns `True`, so as to, in effect, overwrite the file with fresh documentation.


```python
if os.path.isfile(sys.argv[2]):
    os.remove(sys.argv[2])
```

The documentation process begins now by opening the specified Praat file as a read-only file, and the specified Markdown file as a write-only file.

```python
with open(sys.argv[1], 'r') as f:
    with open(sys.argv[2], 'w') as w:
```

The writing process begins by writing the sole first-level header in the Markdown file. If the user has passed in a title as an argument, it will be used. Otherwise, the name of the passed in Praat file will be used.


```python
        if len(sys.argv) == 4:
            w.write('# {}\n\n'.format(sys.argv[3]))
        else:
            w.write('# {}\n\n'.format(os.path.basename(sys.argv[1])))
```

The general parsing strategy is to consume each line in the file until there are no more, catching each of the lines to be extracted and storing them in a list in the proper section of a `dict`. This first section only makes use of the description portion of the `dict`, since it's only reading in the script description contained at the top of the file.

The first set of parsing is set up by initializing the iterator, container variables, and helper variables.

```python
        lines = iter(f.readlines())
        data = {'desc': [''], 'params': list(), 'calcs': list()}
        eof = False
        have_written_script_desc = False
        have_written_procedures_header = False
        curr_adding_to = 'desc'
        new_desc_next = False
```

## Parsing through the File

Now the parsing begins. For each comment line, the indicator is then stripped off and then processed. The parser first checks to see if the line is tagged with `@param` and processes it if so. This processing is setting the `curr_adding_to` variable to reflect the current environment, and then formatting the line to be appropriate for the Markdown file (making it bulleted list entry, italicizing the variable name, and escaping the underscores and dollar signs in the variable name).

```python
        try:
            while not eof:
                line = next(lines)
                if line.startswith('#\''):
                    line = line[2:]
                    
                    if line.strip().startswith('@param'):
                        new_desc_next = False
                        curr_adding_to = 'params'
                        line = line.replace('@param', '*')
                        linelist = line.split()
                        linelist[1] = '*{}*'.format(linelist[1])
                        linelist[1] = linelist[1].replace('_', '\_')
                        linelist[1] = linelist[1].replace('$', '\$')
                        line = ' '.join(linelist)
                        data[curr_adding_to].append(line.strip())
            
```

Similarly, if the line is not a line tagged with `@param`, the parser then checks if the line is tagged with `@calc` and repeats the same process of flagging, formatting, and adding to the `dict` as above.


```python
                    elif line.strip().startswith('@calc'):
                        new_desc_next = False
                        curr_adding_to = 'calcs'
                        line = line.replace('@calc', '*')
                        linelist = line.split()
                        linelist[1] = '*{}*'.format(linelist[1])
                        linelist[1] = linelist[1].replace('_', '\_')
                        linelist[1] = linelist[1].replace('$', '\$')
                        line = ' '.join(linelist)
                        data[curr_adding_to].append(line.strip())
```

Finally, if neither of the above cases fit, the parser will add a description to the current procedure or concatenate the current line with the currently being generated item, whichever is appropriate.

1. The parser checks if the line is blank. If it is, it will set the flag to set a description for the upcoming piece of text.

2. If the line is not blank, the description portion will be added to; this will be the general procedure description if the `curr\_adding\_to` variable is still set to `desc`, or the appropriate `calc` or `param` section if not.

3. If neither of the above cases fit, the description is not being added to, but rather, there is a continuation of the previous entry on the next line. That line is concatenated with the currently being generated item.

```python
                    else:
                        list_index = max(0, len(data[curr_adding_to])-1)
                        if line.strip() == '':
                            new_desc_next = True

                        elif new_desc_next:
                            if curr_adding_to == 'desc':
                                data[curr_adding_to].append(line.strip())
                            else:
                                toadd = '> {}'.format(line.strip())
                                data[curr_adding_to].append(toadd)

                            new_desc_next = False

                        else:
                            curr = data[curr_adding_to][list_index]
                            repl = '{} {}'.format(curr, line.strip())
                            data[curr_adding_to][list_index] = repl
```

If the line did not initially start with a comment indicator and does begin with "procedure", the program then formats the header for the procedure with its name and comma-separated arguments.

```python
                elif line.strip().startswith('procedure'):
                    name = line.replace('procedure', '').strip()
                    namelist = name.split()
                    name = ' '.join(['*{}*'
                                     .format(n)
                                     if namelist.index(n)
                                     == len(namelist) - 1
                                     else '*{}*,'
                                     .format(n.replace(',', ''))
                                     if namelist.index(n) >= 1
                                     else n for n in namelist])
                    name = name.replace('_', '\_').replace('$', '\$')
```
The program then writes the procedures header if it hasn't been already, then  the procedures name and the rest of the description for the current procedure to file, with proper header, text, and whitespace formatting, making sure to also remove any extraneous whitespace that may have been added when reading in the descriptions. The whitespace formatting is such that there will be two newline characters between any two comments, and none at the end of the last comment, so that when the end of the file is reached, a single newline character may be written to finish off the file and have no extra whitespace at the end. Additionally, a portion is only written if there is at least one item in its respective `list` in the `dict`.

```python
                    if not have_written_procedures_header:
                        w.write('\n\n## Procedures')
                        have_written_procedures_header = True
                        
                    w.write('\n\n### {}\n\n'.format(name))
                    
                    currlen = len(data['desc'])
                    if currlen > 0:
                        if currlen > 1:
                            for d in data['desc'][:currlen-1]:
                                w.write('{}\n\n'.format(d.strip()))
                        w.write(data['desc'][currlen-1].strip())
                    
                    currlen = len(data['params'])
                    if currlen > 0:
                        w.write('\n\n#### Parameters\n\n')
                        if currlen > 1:
                            for p in data['params'][:currlen-1]:
                                w.write('{}\n\n'.format(p.strip()))
                        w.write(data['params'][currlen-1].strip())
                        
                    currlen = len(data['calcs'])
                    if currlen > 0:
                        w.write('\n\n#### Calculates\n\n')
                        if currlen > 1:
                            for c in data['calcs'][:currlen-1]:
                                w.write('{}\n\n'.format(c.strip()))
                        w.write(data['calcs'][currlen-1].strip())
```

The environment is then reset to allow the next procedure to be documented.

```python
                            
                    data = {'desc': [''], 'params': list(), 'calcs': list()}
                    name = ''
                    curr_adding_to = 'desc'
                    new_desc_next = False
```

Otherwise, if the line is not blank and does not start with a regular Praat comment indicator, then this is a line of code that is not declaring a procedure, so it must be the end of the general script description section. The description is written to file as above, but this time ensuring that a description comment is not printed if it is blank.

```python
                elif not line.strip() == '' and not line.strip().startswith('#'):
                    
                    currlen = len(data['desc'])
                    if currlen > 0:
                        if currlen > 1:
                            for d in [d for d in data['desc'][:currlen-1]
                                if not d == '']:
                                w.write('{}\n\n'.format(d.strip()))
                        w.write(data['desc'][currlen-1].strip())
                    
                    currlen = len(data['params'])
                    if currlen > 0:
                        w.write('\n\n## Arguments to the Script\n\n')
                        if currlen > 1:
                            for p in data['params'][:currlen-1]:
                                w.write('{}\n\n'.format(p.strip()))
                        w.write(data['params'][currlen-1].strip())
                        
                    currlen = len(data['calcs'])
                    if currlen > 0:
                        w.write('\n\n## Script Calculations\n\n')
                        if currlen > 1:
                            for c in data['calcs'][:currlen-1]:
                                w.write('{}\n\n'.format(c.strip()))
                        w.write(data['calcs'][currlen-1].strip())
```

The environment is then reset to be ready for the next set of documentation that is encountered.

```python
                    have_written_script_desc = True
                    data = {'desc': [''], 'params': list(), 'calcs': list()}
                    name = ''
                    curr_adding_to = 'desc'
                    new_desc_next = False
```

When the parser reaches the end of the file, it will stop, flag the end of file, write a final newline, and write the documentation to file. This will only write the general script description to file, however, as the other pieces of documentation should always be preceding a procedure declaration (which means they will never be the end of the file).

```python
        except StopIteration:
            eof = True
            w.write('\n')
            
            if not have_written_script_desc:
                if len(data['desc']) > 0:
                    for d in data['desc']:
                        w.write('{}\n\n'.format(d.strip()))
                
                if len(data['params']) > 0:
                    w.write('## Arguments to the Script\n\n')
                    for p in data['param']:
                        w.write('{}\n\n'.format(p.strip()))
                        
                if len(data['calcs']) > 0:
                    w.write('## Script Calculations\n\n')
                    for c in data['calc']:
                        w.write('{}\n\n'.format(c.strip()))
```

# A Minimal Example

A sample file, `script.praat` is distributed with this parser. It contains the following text:

    # Copyright 2016 Matthew C. Kelley
    # Written at the University of Alberta
    #
    # A sample Praat script to demonstrate the PraatDocExtractor
    #
    # This file is part of PraatDocExtractor.
    #
    # PraatDocExtractor is free software: you can redistribute it and/or modify
    # it under the terms of the GNU Lesser General Public License as published by
    # the Free Software Foundation, version 2.1.
    #
    # PraatDocExtractor is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    # GNU General Public License for more details.
    #
    # You should have received a copy of the GNU Lesser General Public License, version 2.1
    # along with PraatDocExtractor. If not, see <http://www.gnu.org/licenses/>.
    
    #' This is a sample script for the document generation.
    #'
    #' We can see that there will be some line breaks here.
    #'
    #'
    #' This is testing how many line breaks you can put in.
    #'
    #'
    #' @param num1 A positive real number that 3 will be added to
    #' @param blahblah$ A string that will be printed out and doubled
    #'
    #' @calc mysum The sum of `num1` and 3
    #' @calc mystring$ The `blahblah` variable with the string "blah" at the end of it

    ################
    # script.praat #
    ################

    form Sample form
        positive num1 3
        text blahblah test
    endform

    var = 2+3
    # Comments that will be
    s$ = "hello"
    # Ign
    # oredddd

    @test: num1, blahblah$
    mysum = test.val

    @test2: "Hello, world!"

    @test3: blahblah$
    mystring$ = test2.val$

    #' A sample procedure that performs simple calculations.
    #'
    #' @param .var The value to add three to.
    #' 
    #' Here's more information about `.var`
    #'
    #'
    #' And more...
    #' 
    #' @calc .val The calculated result of adding 3 to `.var`.
    #' @calc .out$ A repetition of the `.p$` variable.
    #'
    #' @param .p$ The string value to be repeated.
    procedure test: .var, .p$
        .intermediate = .var + 1
        .val = .intermediate + 2
        .out$ = .p$ + .p$
    endproc

    #' Writes the passed in string value or variable to the info line
    #'
    #' @param .val$ String to be written to the info line
    procedure test2: .val$
        writeInfoLine: .val$
    endproc

    #' A third procedure! It writes a passed value to the InfoLine.
    #'
    #' It also contains a multiparagraph and multiline
    #' description.
    #'
    #' @param .my$ The string to add "blah" to
    procedure test3: .my$
        .val$ = .my$ + "blah"
    endproc
    
Look, first, at how the parser will skip over regular Praat comments, like the license information here, when looking for the general script description. Next, notice that the last parameter for the `test` procedure comes after all the calculations. This will not matter, however, as it will be ordered with the other parameter when the Markdown file is written. Additionally, note the way that the paragraph breaks work. While it would be preferable to have the whitespace be uniform at two newline characters to signify a new paragraph, the parser will not choke when encountering more. When the parser script is run on this `script.praat` file with the following command

```bash
python praatdocextractor.py script.praat script.md
```

The following output is what is produced in the specified `script.md` file:

    # script.praat

    This is a sample script for the document generation.

    We can see that there will be some line breaks here.

    This is testing how many line breaks you can put in.

    ## Arguments to the Script

    * *num1* A positive real number that 3 will be added to

    * *blahblah\$* A string that will be printed out and doubled

    ## Script Calculations

    * *mysum* The sum of `num1` and 3

    * *mystring\$* The `blahblah` variable with the string "blah" at the end of it

    ## Procedures

    ### test: *.var*, *.p\$*

    A sample procedure that performs simple calculations.

    #### Parameters

    * *.var* The value to add three to.

    > Here's more information about `.var`

    > And more...

    * *.p\$* The string value to be repeated.

    #### Calculates

    * *.val* The calculated result of adding 3 to `.var`.

    * *.out\$* A repetition of the `.p$` variable.

    ### test2: *.val\$*

    Writes the passed in string value or variable to the info line

    #### Parameters

    * *.val\$* String to be written to the info line.

    ### test3: *.my\$*

    A third procedure! It concatenates a passed value and "blah"

    It also contains a multiparagraph and multiline description.

    #### Parameters

    * *.my\$* The string to add "blah" to.

    #### Calculates

    * *.val\$* The concatenation of `.my$` and "blah"
    
This resulting Markdown file can be distributed as is for other users, converted to HTML or a PDF using a tool such as [Pandoc](http://pandoc.org/), or even used as page for a documentation system such as [MkDocs](http://www.mkdocs.org/) if converted to CommonMark (which Pandoc can do). Any further formatting that needs to be done on the part of the user should be done at this point as well.

# Limitations

Currently, the parser will not perform any explicit syntactic checks, but rather will operate solely on the input its given and produce the output that would be generated by the input. This means that any formatting errors, mistakes in the documentation code, unescaped characters, etc., will be transferred to the resulting Markdown file.

Additionally, the program only works on one file at a time at the moment, not all files within a directory or similar such behavior.

Both of these are areas that could be addressed in future revisions of the program, should the desire be great enough.
