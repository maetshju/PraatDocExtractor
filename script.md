# script.praat

This is a sample script for the document generation.

We can see that there will be some line breaks here.

This is testing how many line breaks you can put in.

## Arguments to the Script

* *num1* A positive real number that 3 will be added to

* *blahblah\$* A string that will be printed out and doubled

## Script Calculations

* *mysum* The sum of `num1` and 3

* *mystring\$* The `blahblah` variable with the string "blah" at the end of it

## Procedures

### test: *.var*, *.p\$*

A sample procedure that performs simple calculations.

#### Parameters

* *.var* The value to add three to.

> Here's more information about `.var`

> And more...

* *.p\$* The string value to be repeated.

#### Calculates

* *.val* The calculated result of adding 3 to `.var`.

* *.out\$* A repetition of the `.p$` variable.

### test2: *.val\$*

Writes the passed in string value or variable to the info line

#### Parameters

* *.val\$* String to be written to the info line.

### test3: *.my\$*

A third procedure! It concatenates a passed value and "blah"

It also contains a multiparagraph and multiline description.

#### Parameters

* *.my\$* The string to add "blah" to.

#### Calculates

* *.val\$* The concatenation of `.my$` and "blah"
